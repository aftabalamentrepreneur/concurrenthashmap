import java.util.*;
import java.util.concurrent.*;
public class ConcurrentHashMapDemo extends Thread{

	private static ConcurrentHashMap<Integer, String> concurrentHashMap=new ConcurrentHashMap<Integer, String>();
	public static void main(String a[]) throws InterruptedException{
		concurrentHashMap.put(100, "A");
		concurrentHashMap.put(101, "B");
		concurrentHashMap.put(102, "C");
		ConcurrentHashMapDemo thread=new ConcurrentHashMapDemo();
		thread.start();
		Set set=concurrentHashMap.entrySet();
		Iterator iterator=set.iterator();
		while(iterator.hasNext()){
			Map.Entry map=(Map.Entry)iterator.next();
			System.out.println("Key: "+map.getKey()+" Value: "+map.getValue());
			Thread.sleep(3000);
		}
	}
	
	public void run(){
		try{
			Thread.sleep(2000);
			concurrentHashMap.put(103, "D");	
		} 
		catch(InterruptedException IE){
			IE.printStackTrace();
		}
	}
} 
