import java.util.*;
import java.util.concurrent.*;
public class ArrayListDemo extends Thread{

	private static ArrayList arrayList=new ArrayList();
	public static void main(String a[]) throws InterruptedException{
		arrayList.add("A");
		arrayList.add("B");
		arrayList.add("C");
		ArrayListDemo thread=new ArrayListDemo();
		thread.start();
		Iterator iterator=arrayList.iterator();
		while(iterator.hasNext()){
			String value=(String)iterator.next();
			System.out.println("Value: "+value);
			Thread.sleep(3000);
		}
	}
	
	public void run(){
		try{
			Thread.sleep(2000);
			arrayList.add("D");	
		} 
		catch(InterruptedException IE){
			IE.printStackTrace();
		}
	}
} 
